var App = function() {

    var content = $("#content"),
        elephant = $(".elephant"),
        lineCount = 4,
        elephantCount = 3,
        imgWidthHeight = 180,
        colsLength = Math.pow(lineCount, 2);

    content.css("width", (lineCount * imgWidthHeight) + 1 + "px");
    content.css("height", (lineCount * imgWidthHeight) + 1 + "px");

    for (var i = 1; i <= colsLength; i++) {
       var smallDivs = $("<div>").addClass("cols");
       content.append(smallDivs);
    }
    var cols = $(".cols");

    init();

    var count = elephantCount + 1;

    for (var i = 1; i <= count; i++) {
        var random = parseInt(Math.random() * cols.length);
        var col = cols.get(random);

        if (i <= count - 1) {
            if (!$(col).hasClass("elephant")) {
                $(col).addClass("elephant");
            } else {
                i--;
            }
        } else {
            if(!$(col).hasClass("elephant")) {
                $(col).addClass("evil");
            } else {
                i--;
            }
        }
    }

    function init(){
        for(var i = 1; i <= cols.length; i++) {
            $(".cols").eq(i-1)
                .attr("data-id", i);
        }
    }

    $(document).keyup(function(event) {
        var dataId = $('.evil').attr('data-id');
        var parse = parseInt(dataId);
        var nextColId;
        var current;

        if (event.which === 37) {
            nextColId = parse - 1;
        } else if (event.which === 38) {
            nextColId = parse - lineCount;
        } else if (event.which === 39) {
            nextColId = parse + 1;
        } else if (event.which === 40) {
            nextColId = parse + lineCount;
        }

        if(typeof nextColId !== "undefined") {
            console.log(5);
            current = $('.cols[data-id=' + nextColId + ']');
            sides(current);
        }


        if ($(".elephant").length == 0) {
             $("#content").hide();
             $(".last").show();
            if (event.which === 13) {
                location.reload();
            }
        }
    });

    function sides(current) {
        var evil = $(".evil");

        if (current.length !== 0) {
            evil.removeClass('evil');

            if (current.hasClass('elephant')) {
                current.removeClass("elephant");
            }

            current.addClass("evil");
        }

    }

    };

$(App);

